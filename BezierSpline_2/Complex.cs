﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BezierSpline_2
{
    public class Complex
    {
        private double __Im, __Re;

        public static Complex I = new Complex(1, 0);
        public static Complex Zero = new Complex(0, 0);
        public Complex() { __Im = 0; __Re = 0; }
        public Complex(double im, double re) { __Im = im; __Re = re; }

        public static implicit operator Complex(double d)
        {
            return new Complex(0, d);
        }
        public static bool operator ==(Complex cpl1, Complex cpl2)
        {
            return !(cpl1 != cpl2);
        }
        public override bool Equals(object obj)
        {
            return (this == (Complex)obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode() + 1;
        }
        public static bool operator !=(Complex cpl1, Complex cpl2)
        {
            double s_im = Math.Abs(cpl1.Im - cpl2.Im);
            double s_re = Math.Abs(cpl1.Re - cpl2.Re);

            return (s_im > Epsilon || s_re > Epsilon);
        }
        public static Complex operator +(Complex cpl1, Complex cpl2)
        {
            return new Complex(cpl1.Im + cpl2.Im, cpl1.Re + cpl2.Re);
        }
        public static Complex operator -(Complex cpl)
        {
            return new Complex(-cpl.Im, -cpl.Re);
        }
        public static Complex operator -(Complex cpl1, Complex cpl2)
        {
            return new Complex(cpl1.Im - cpl2.Im, cpl1.Re - cpl2.Re);
        }

        public static Complex operator *(Complex cpl1, Complex cpl2)
        {
            return new Complex(cpl1.Im * cpl2.Re + cpl2.Im * cpl1.Re, cpl1.Re * cpl2.Re - cpl1.Im * cpl2.Im);
        }

        public static Complex operator *(Complex cpl1, double d)
        {
            return new Complex(d * cpl1.Im, d * cpl1.Re);
        }

        public static Complex operator /(Complex cpl, double d)
        {
            return new Complex(cpl.Im / d, cpl.Re / d);
        }

        public static Complex operator /(Complex cpl1, Complex cpl2)
        {
            return (cpl1 * Conjugation(cpl2)) / Magnitude2(cpl2);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Complex Sqrt(double q)
        {
            double abs_q = Math.Sqrt(Math.Abs(q));
            if (q >= 0) { return new Complex(0, abs_q); }
            return new Complex(abs_q, 0);
        }
        /// <summary>
        /// Compute conjugation of the complex
        /// </summary>
        /// <param name="cpl"></param>
        /// <returns></returns>
        public static Complex Conjugation(Complex cpl)
        {
            return new Complex(-cpl.Im, cpl.Re);
        }
        /// <summary>
        /// Get modulus of the complex
        /// </summary>
        /// <param name="cpl"></param>
        /// <returns></returns>
        public static double Magnitude(Complex cpl)
        {
            return Math.Sqrt(cpl.Im * cpl.Im + cpl.Re * cpl.Re);
        }
        /// <summary>
        /// Compute the square of complex magnitude
        /// </summary>
        /// <param name="cpl"></param>
        /// <returns></returns>
        public static double Magnitude2(Complex cpl)
        {
            return (cpl.Im * cpl.Im + cpl.Re * cpl.Re);
        }
        /// <summary>
        /// Set/get imagine part of a complex
        /// </summary>
        public double Im { get { return __Im; } set { __Im = value; } }
        /// <summary>
        /// Set/get real part of a complex
        /// </summary>
        public double Re { get { return __Re; } set { __Re = value; } }
        /// <summary>
        /// Get the magnitude of the complex
        /// </summary>
        public double Mag { get { return Magnitude(this); } }
        /// <summary>
        /// Get the argument of the complex
        /// </summary>
        public double Arg { get { return Math.Atan2(__Im, __Re); } }
        /// <summary>
        /// Whether the complex is a real number
        /// </summary>
        public bool IsReal { get { return ((__Im < Epsilon) && (__Im > -Epsilon)); } }
        /// <summary>
        /// Whether the complex is a imagine number
        /// </summary>
        public bool IsImagine { get { return ((__Re < Epsilon) && (__Re > -Epsilon)); } }
        public const double Epsilon = 1e-10;
        /// <summary>
        /// Convert ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            double _absIm = Math.Abs(__Im);
            string s_re = string.Format("{0:0.000}", __Re);
            string s_im = string.Format("{0:0.000 I}", _absIm);
            string s = s_re;
            s = _absIm > Epsilon ? (__Im > 0 ? (s + " + " + s_im) : (s + " - " + s_im)) : s;
            return s;
        }
        /// <summary>
        /// r(cos(phi) + i.sin(phi) = r
        /// </summary>
        /// <param name="r">Magnitude of the complex </param>
        /// <param name="phi">Argument of the complex </param>
        /// <returns></returns>
		public static Complex Expo(double r, double phi)
        {
            return new Complex(r * Math.Sin(phi), r * Math.Cos(phi));
        }
        /// <summary>
        /// pow(cpl, n)
        /// </summary>
        /// <param name="cpl">The complex number</param>
        /// <param name="n">exponential number</param>
        /// <returns></returns>
        public static Complex Pow(Complex cpl, double n)
        {
            double r = cpl.Mag, phi = cpl.Arg;
            r = Math.Pow(r, n);
            phi *= n;
            return Complex.Expo(r, phi);
        }
        /// <summary>
        /// pow(cpl, 1/n)
        /// </summary>
        /// <param name="cpl"></param>
        /// <param name="n"></param>
        /// <param name="roots"></param>
        public static void Pow_Inverse(Complex cpl, uint n, out Complex[] roots)
        {
            roots = new Complex[n];
            double r = cpl.Mag, phi = cpl.Arg;
            r = Math.Pow(r, 1.0/n);
            if (n == 0)
            {
                roots[0] = new Complex(0, 1);
                return;
            }
            for (int i = 0; i < n; i++)
            {
                roots[i] = Complex.Expo(r, (phi + 2 * i * Math.PI) / n);
            }
        }
        /// <summary>
        /// Giai phuong trinh bac 2
        /// </summary>
        /// <param name="a2">t^2</param>
        /// <param name="a1">t^1</param>
        /// <param name="a0">t^0</param>
        /// <param name="roots">Nghiem cua phuong trinh</param>
        public static void SolveEq(double a2, double a1, double a0, out Complex[] roots)
        {
            double delta = a1 * a1 - 4 * a2 * a0;
            roots = new Complex[2];
            roots[0] = (-a1 + Sqrt(delta)) / (2 * a2);
            roots[1] = (-a1 - Sqrt(delta)) / (2 * a2);
        }
        /// <summary>
        /// Giai phuong trinh bac 3
        /// </summary>
        /// <param name="a3">t^3</param>
        /// <param name="a2">t^2</param>
        /// <param name="a1">t^1</param>
        /// <param name="a0">t^0</param>
        /// <param name="roots">Nghiem cua pt</param>
        public static void SolveEq(double a3, double a2, double a1, double a0, out Complex[] roots)
        {
            double a = a2 / a3, b = a1 / a3, c = a0 / a3;
            Complex p = b - a * a / 3, q = c + (2 * a * a * a - 9 * a * b) / 27;
            Complex delta = Complex.Pow(q, 2) / 4 + Complex.Pow(p, 3) / 27;
            Complex sqrt_delta = Complex.Pow(delta, 0.5);
            Complex[] u1;
            Complex.Pow_Inverse(q / 2 + sqrt_delta, 3, out u1);
            int ind = 0;
            roots = new Complex[3];
            for (int i = 0; i < 3; i++)
            {
                roots[i] = new Complex();
                if (u1[i] != Complex.Zero)
                {
                    roots[ind] = p / (3 * u1[i]) - u1[i] - a / 3;
                    ind++;
                }
            }
            if (ind == 3) { return; }
            Complex[] u2;
            Complex.Pow_Inverse(q / 2 - sqrt_delta, 3, out u2);
            for (int i = 0; i < 3; i++)
            {
                if (u2[i] != Complex.Zero)
                {
                    Complex tmp = p / (3 * u2[i]) - u2[i] - a / 3;
                    bool isDuplicate = false;
                    for (int j = 0; j < ind; j++)
                    {
                        if (roots[j] == tmp) { isDuplicate = true; break; }
                    }
                    if (!isDuplicate)
                    {
                        roots[ind] = tmp;
                        ind++;
                    }
                }
            }
        }
    }
}
