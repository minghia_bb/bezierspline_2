﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BezierSpline_2
{
    public partial class PropertiesDlg : Form
    {
        float __n1 = 1, __n2 = 1.5f;
        bool __isManual_Lighting = false;
        int __NumOfPoint = 10;
        float __range = 30;
        public PropertiesDlg()
        {
            InitializeComponent();
        }
        public PropertiesDlg(float n1, float n2, bool IsManualLighting, int NumofPoint, float range)
        {
            InitializeComponent();
            __n1 = n1; __n2 = n2;
            __range = range;
            __NumOfPoint = NumofPoint;
            //__isManual_Lighting = IsManualLighting;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            float tmp;
            int n_tmp;
            if (!float.TryParse(txt_n1.Text, out tmp) || tmp <= 0)
            {
                MessageBox.Show("Chiet suat 1 is Invalid!", "Error", MessageBoxButtons.OK);
                txt_n1.Focus();
                return;
            }
            __n1 = tmp;
            if (!float.TryParse(txt_n2.Text, out tmp) || tmp <= 0)
            {
                MessageBox.Show("Chiet suat 2 is Invalid!", "Error", MessageBoxButtons.OK);
                txt_n2.Focus();
                return;
            }
            __n2 = tmp;
            //__isManual_Lighting = ck_ManualLighting.Checked;
            if (!__isManual_Lighting)
            {
                if ((!int.TryParse(txt_NumOfPoint.Text, out n_tmp) || n_tmp <= 0))
                {
                    MessageBox.Show("Num of Points is Invalid!", "Error", MessageBoxButtons.OK);
                    txt_NumOfPoint.Focus();
                    return;
                }
                __NumOfPoint = n_tmp;
            }
            if (!float.TryParse(txt_Range_max.Text, out tmp))
            {
                MessageBox.Show("Range max is Invalid!", "Error", MessageBoxButtons.OK);
                txt_Range_max.Focus();
                return;
            }
            __range = tmp;

            Close();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            switch ((int)e.KeyChar)
            {
                case 13: // enter
                    e.Handled = true;
                    this.btn_Ok.PerformClick();
                    break;
                default: break;
            }
            base.OnKeyPress(e);
        }

        public void Properties_Load(object sender, EventArgs e)
        {
            txt_n1.Text = String.Format("{0:0.0}", __n1);
            txt_n2.Text = String.Format("{0:0.0}", __n2);
            txt_NumOfPoint.Text = String.Format("{0}", __NumOfPoint);
            txt_Range_max.Text = String.Format("{0:0.0}", __range);
            
            ck_ManualLighting.CheckState = __isManual_Lighting ? CheckState.Checked : CheckState.Unchecked;
            txt_NumOfPoint.Enabled = !__isManual_Lighting;
            ck_ManualLighting.Visible = false;
        }

        public float N1
        {
            get { return __n1; }
            set { __n1 = value; }
        }
        public float N2
        {
            get { return __n2; }
            set { __n2 = value; }
        }

        public bool IsManual_Lighting
        {
            get { return __isManual_Lighting; }
            set { __isManual_Lighting = value; }
        }

        private void ManualLight_CheckChanged(object sender, EventArgs e)
        {
            //__isManual_Lighting = ck_ManualLighting.Checked;
            //txt_NumOfPoint.Enabled = !__isManual_Lighting;
        }

        public int NumOfPoint
        {
            get { return __NumOfPoint; }
            set { __NumOfPoint = value; }
        }
        public float Range
        {
            get { return __range; }
            set { __range = value; }
        }
    }
}
