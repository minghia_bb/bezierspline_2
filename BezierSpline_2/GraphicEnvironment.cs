﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace BezierSpline_2
{
    public class PointEx
    {
        private PointF __point = new PointF();
        private PointF __leftMouse_point = new PointF();
        private bool __leftMouse_Clicked = false;
        private bool __isSourceLight = false;
        private bool __isVisible = true;
        private Image __img = null;

        private Color __normal_color = Color.Black;
        private Color __chosen_color = Color.Red;
        public PointEx()
        {
            __point.X = __point.Y = 0.0f;
        }

        public PointEx(double x, double y)
        {
            __point.X = (float)x;
            __point.Y = (float)y;
        }
        public PointEx(bool isSourceLight)
        {
            __point.X = 0; __point.Y = 0;
            __isSourceLight = isSourceLight;
            __isVisible = !__isSourceLight;
            w_h = __isSourceLight ? 30 : 8;
            if (__isSourceLight)
            {
                __normal_color = Color.Green;
            }
        }
        public PointEx(PointF point)
        {
            X = point.X;
            Y = point.Y;
        }
        public static int Comparision(PointEx p1, PointEx p2)
        {
            float sub = p1.X - p2.X;
            if (sub >= 0) return 1;
            else return -1;
        }
        float w_h = 8;
        public bool IsChoosen()
        {
            if (!__isVisible) { return false; }
            float x, y;
            x = __point.X - __leftMouse_point.X;
            y = __point.Y - __leftMouse_point.Y;
            if (__isSourceLight)
            {
                if (__img != null)
                {
                    float w = __img.Width / s_w, h = __img.Height / s_h;
                    float min_of_w_h = w > h ? h : w;
                    return (bool)((x > -min_of_w_h) && (x < min_of_w_h) &&
                        (y > -min_of_w_h) && (y < min_of_w_h));
                }
            }
            return (bool)((x * x + y * y) < w_h * w_h);
        }
        public float Wid_Hei
        {
            get { return w_h; }
            set { w_h = value; }
        }
        public float X
        {
            get { return __point.X; }
            set { __point.X = value; }
        }
        public float Y
        {
            get { return __point.Y; }
            set { __point.Y = value; }
        }
        public bool Visible
        {
            get { return __isVisible; }
            set { __isVisible = value; }
        }
        public Image Image_s
        {
            get { return __img; }
            set { __img = value; }
        }
        public bool SoureLight
        {
            get { return __isSourceLight; }
            set { __isSourceLight = value; }
        }
        public void LeftMouse_Down(PointF point)
        {
            if (!__isVisible) { return; }
            __leftMouse_point.X = point.X;
            __leftMouse_point.Y = point.Y;
            __leftMouse_Clicked = true;
        }
        public void LeftMouse_Up() { __leftMouse_Clicked = false; }
        public void Drag(PointF point, Graphics grap, Pen p, Color backgrdColor)
        {
            if (!IsChoosen()) { return; }
            if (!__leftMouse_Clicked) { return; }
            Remove(grap, backgrdColor);
            __point.X += point.X - __leftMouse_point.X;
            __point.Y += point.Y - __leftMouse_point.Y;
            __leftMouse_point = point;
            Draw(grap, p, backgrdColor);
        }

       // float w_h = 8;
        public void Remove(Graphics grap, Color backgrdColor)
        {
            if (!__isVisible) { return; }
            if (__isSourceLight) { Remove_SourceLight(grap, backgrdColor); }
            else { Remove_NormalPoint(grap, backgrdColor); }
        }
        public void Draw(Graphics grap, Pen pen, Color backgrdColor)
        {
            if (!__isVisible) { return; }
            if (__isSourceLight) { Draw_SourceLight(grap, pen, backgrdColor); }
            else { Draw_NormalPoint(grap, pen, backgrdColor); }
        }
        private void Draw_NormalPoint(Graphics grap, Pen pen, Color backgrdColor)
        {
            //Remove(grap, backgrdColor);

            if (IsChoosen())
            {
                grap.FillEllipse(new SolidBrush(__chosen_color),
                    __point.X - w_h / 2, __point.Y - w_h / 2,
                    w_h, w_h);
            }
            else
            {
                grap.FillEllipse(new SolidBrush(__normal_color),
                    __point.X - w_h / 2, __point.Y - w_h / 2,
                    w_h, w_h);
            }
        }
        private void Remove_NormalPoint(Graphics grap, Color backgrdColor)
        {
            grap.FillEllipse(new SolidBrush(backgrdColor),
                __point.X - w_h / 2, __point.Y - w_h / 2,
                    w_h, w_h);
        }

        private float s_w = 1.0f, s_h = 1.0f;
        private void Draw_SourceLight(Graphics grap, Pen pen, Color backgrdColor)
        {
            if (__img == null) {
                Draw_NormalPoint(grap, pen, backgrdColor);
                return; 
            }
            if (!__isVisible) { return; }
            float w = __img.Width / s_w, h = __img.Height / s_h;
            grap.DrawImage(__img,
                __point.X - w / 2,
                __point.Y - h / 2,
                w, h);

            Draw_NormalPoint(grap, pen, backgrdColor);
        }

        private void Remove_SourceLight(Graphics grap, Color backgrdColor)
        {
            if (__img == null) {
                Remove_NormalPoint(grap, backgrdColor);
                return; 
            }
            float w = __img.Width / s_w, h = __img.Height / s_h;
            grap.FillRectangle(new SolidBrush(backgrdColor),
                __point.X - w / 2,
                __point.Y - h / 2,
                w, h);
            //__isVisible = false;
        }
    }

    public class GraphicEnvironment
    {
        public Graphics __graphics = null;
        private Pen __pen = new Pen(Color.Black, 1.0f);
        private Color __backgrdColor;// = Color.White;Color.Azure; 
        public Color __otherColor  = Color.White;// mau cho mien co chieu suat cao hon
        private PointEx __sourceLight = new PointEx(true);
        private PointEx __direction_point = new PointEx(true); // firt init, is Source light
        private List<PointEx> __points = new List<PointEx>();
        public float XScale { get; set; }
        public float YScale { get; set; }
        public PointEx Offset = new PointEx();
        public int Width { get; set; }
        public int Height { get; set; }
        public PointEx SourceLight { get { return __sourceLight; } }
        public PointEx DiectionPoint { get { return __direction_point; } }
        public GraphicEnvironment(Graphics graphics, PointF Off_set, Color backgrdColor)
        {
            __graphics = graphics;
            Offset.X = Off_set.X;
            Offset.Y = Off_set.Y;
            XScale = 1.0f;
            YScale = 1.0f;
            __backgrdColor = backgrdColor;

            __direction_point.Wid_Hei = 5;
            __direction_point.SoureLight = false;
            __direction_point.Visible = false;
        }
        public void RefreshGraph(Graphics graphics)
        {
            __graphics = graphics;
        }
        public void AddPoint(PointF point)
        {
            PointEx pex = new PointEx(point);
            __points.Add(pex);
            Render();
        }
        public void Remove_PointSelected()
        {
            int index = AnyPoint_Selected();
            if (index >= 0)
            {
                __points.ElementAt(index).Remove(__graphics, __backgrdColor);
                __points.RemoveAt(index);
            }
            if (index == -1)
            {
                Remove_SourceLight();
                __sourceLight.Visible = false;
                return;
            }
            if (index == -2)
            {
                Remove_DirectionPoint();
                return;
            }
            Render();
        }
        public void CleanScreen()
        {
            __graphics.Clear(__backgrdColor);
            __points.Clear();
            __sourceLight.Visible = false;
            __direction_point.Visible = false;
        }

        public void LeftMouse_Down(PointF point)
        {
            if (__points != null)
            {
                for (int i = 0; i < __points.Count; i++)
                {
                    __points.ElementAt(i).LeftMouse_Down(point);
                }
            }
            __sourceLight.LeftMouse_Down(point);
            __direction_point.LeftMouse_Down(point);
        }
        public void LeftMouse_Up()
        {
            if (__points != null)
            {
                for (int i = 0; i < __points.Count; i++)
                {
                    __points.ElementAt(i).LeftMouse_Up();
                }
            }
            __sourceLight.LeftMouse_Up();
            __direction_point.LeftMouse_Up();
        }
        public void Drag(PointF point)
        {
            if (__points != null)
            {
                for (int i = 0; i < __points.Count; i++)
                {
                    __points.ElementAt(i).Drag(point, __graphics, __pen, __backgrdColor);
                }
            }
            __sourceLight.Drag(point, __graphics, __pen, __backgrdColor);
            __direction_point.Drag(point, __graphics, __pen, __backgrdColor);
        }

        Region __region;
        private void Update_BezierSpline()
        {
            int pointCount = __points.Count;
            if (pointCount < 2) { return; }
            Pen old = __pen;
            __pen = new Pen(Color.Blue, 4.0f);
            PointF[] cp1, cp2;
            GraphicsPath paths = new GraphicsPath();
            BezierSpline.GetCurveControlPoints(__points, out cp1, out cp2);
            for (int i = 0; i < cp1.Length; ++i)
            {
                PointF knotStart = new PointF(__points[i].X, __points[i].Y);
                PointF knotEnd = new PointF(__points[i + 1].X, __points[i + 1].Y);
                paths.AddBezier(knotStart, cp1[i], cp2[i], knotEnd);
            }
            __graphics.DrawPath(__pen, paths);

            float xmin = __points[0].X, ymin = __points[0].Y;
            float xmax = __points[__points.Count - 1].X, ymax = __points[__points.Count - 1].Y;
            PointF p0 = new PointF(xmin, ymin);
            PointF p1 = new PointF(0, ymin);
            PointF p2 = new PointF(0, this.Height);
            PointF p3 = new PointF(this.Width, this.Height);
            PointF p4 = new PointF(this.Width, ymax);
            PointF p5 = new PointF(xmax, ymax);
            paths.AddLine(p0, p1);
            paths.AddLine(p1, p2);
            paths.AddLine(p2, p3);
            paths.AddLine(p3, p4);
            paths.AddLine(p4, p5);

            __region = new Region(paths);
            __graphics.FillRegion(new SolidBrush(__otherColor), __region);

            __pen = old;
            __graphics.DrawLine(__pen, p0, p1);
            __graphics.DrawLine(__pen, p4, p5);
            
            paths.Reset();
        }

        public void TiaSang(PointF startPoint, PointF lineDir, PointF[] cp1, PointF[] cp2, bool bArrawDrawed = true, int lengthArrow = 20)
        {
            //stream.WriteLine("Calling TiaSang(PointF startPoint, PointF lineDir, PointF[] cp1, PointF[] cp2)");
            //stream.Write("\t");
            //stream.Write("startPoint=[{0:0.00},{1:0.00}]. ", startPoint.X, startPoint.Y);
            //stream.Write("lineDir=[{0:0.00},{1:0.00}]\n\n", lineDir.X, lineDir.Y);
            PointF giao_diem, tiep_tuyen, vectorKxa;
            bool bl;
            while (true)
            {
                bl = TiaSang_Chieu(startPoint, lineDir, cp1, cp2, out giao_diem, out tiep_tuyen);
                if (bl)
                {
                    //GocToi(giao_diem, lineDir, tiep_tuyen, out vectorKxa);
                    if (GocToi(startPoint, giao_diem, tiep_tuyen, out vectorKxa))
                    {
                        //Draw_Arrow2(__pen, giao_diem, tiep_tuyen, 20);
                        float x = tiep_tuyen.Y;
                        float y = -tiep_tuyen.X;
                        if (bArrawDrawed)
                        {
                            Draw_Arrow2(__pen, giao_diem, new PointF(x, y), lengthArrow);
                        }
                        //DrawLine(__pen, giao_diem, vectorKxa);

                        startPoint = giao_diem;
                        lineDir = vectorKxa;
                    }
                    else { break; }
                }
                else { break; }
            }
        }
        public void Render()
        {
            if (__graphics != null && __points != null)
            {
                int pointCount = __points.Count;
                // __n1 - Up
                // __n2 - Down
                if (__n1 > __n2)
                {
                    __backgrdColor = Color.Azure;
                    __otherColor = Color.White;
                }
                else
                {
                    __backgrdColor = Color.White;
                    __otherColor = Color.Azure;
                }
                __graphics.Clear(__backgrdColor);
                __points.Sort(PointEx.Comparision);
                Update_BezierSpline();
                for (int i = 0; i < pointCount; i++)
                {
                    __points.ElementAt(i).Draw(__graphics, __pen, __backgrdColor);
                }
                if (pointCount > 1 && __direction_point.Visible && __sourceLight.Visible)
                {
                    PointF[] cp1, cp2;
                    BezierSpline.GetCurveControlPoints(__points, out cp1, out cp2);
                    PointF vector, point;
                    
                    vector = new PointF(__direction_point.X - __sourceLight.X, __direction_point.Y - __sourceLight.Y);
                    point = new PointF(__sourceLight.X, __sourceLight.Y);
                    
                    int NNN = __NumOfPoints;
                    int lengthArrow = 20;
                    bool bDrawArrow = false;

                    float delta = Range_inDegree / NNN; // degree

                    //if (Range_inDegree < 4) { NNN = 1; }
                    //if (delta < 1.001)
                    //{
                    //    delta = 1;
                    //    NNN = (int)(Range_inDegree / delta);
                    //}
                    //bDrawArrow = delta >= 5;
                    //lengthArrow = bDrawArrow ? lengthArrow : 50;

                    delta *= (float)(Math.PI / 180.0);

                    //stream = System.IO.File.CreateText("log.txt");
                    //stream.WriteLine("\n----------------------\n");
                    if (NNN <= 1)
                    {
                        s = ""; 
                        TiaSang(point, vector, cp1, cp2);
                    }
                    else
                    {
                        s = "";

                        TiaSang(point, vector, cp1, cp2, true, lengthArrow); // center line

                        for (float angle = delta; angle < delta * NNN / 2; angle += delta)
                        {
                            PointF vector1 = new PointF(), vector2 = new PointF();
                            point = new PointF(__sourceLight.X, __sourceLight.Y);
                            float cos_ = (float)Math.Cos(angle);
                            float sin_ = (float)Math.Sin(angle);
                            vector1.X = vector.X * cos_ - vector.Y * sin_;
                            vector1.Y = vector.X * sin_ + vector.Y * cos_;
                            vector2.X = vector.X * cos_ + vector.Y * sin_;
                            vector2.Y = -vector.X * sin_ + vector.Y * cos_;
                            s = "";
                            TiaSang(point, vector1, cp1, cp2, bDrawArrow, lengthArrow);
                            s = "";
                            TiaSang(point, vector2, cp1, cp2, bDrawArrow, lengthArrow);
                        }
                    }
                    //stream.Close();
                }
                Draw_SourceLight(); //__sourceLight.Draw(__graphics, __pen, __backgrdColor);
                Draw_DirectionPoint();
              
            }
        }
        public void Draw_DirectionPoint()
        {
            __direction_point.Draw(__graphics, __pen, __backgrdColor);
            if (__direction_point.Visible)
            {
                Draw_Arrow2(__pen, new PointF(__direction_point.X, __direction_point.Y), new PointF(0, 1), 10);
                Draw_Arrow2(__pen, new PointF(__direction_point.X, __direction_point.Y), new PointF(1, 0), 10);
            }
        }
        public void Remove_DirectionPoint()
        {
            Pen pen = new Pen(__backgrdColor, __pen.Width);
            __direction_point.Remove(__graphics, __backgrdColor);
            Draw_Arrow2(pen, new PointF(__direction_point.X, __direction_point.Y), new PointF(0, 1), 10);
            Draw_Arrow2(pen, new PointF(__direction_point.X, __direction_point.Y), new PointF(1, 0), 10);
            __direction_point.Visible = false;
        }
        public void Draw_SourceLight()
        {
            if (__sourceLight.Visible)
            {
                //Draw_Arrow2(__pen, new PointF(__sourceLight.X, __sourceLight.Y), new PointF(0, 1), 30);
                //Draw_Arrow2(__pen, new PointF(__sourceLight.X, __sourceLight.Y), new PointF(1, 0), 30);
            }
            __sourceLight.Draw(__graphics, __pen, __backgrdColor);
        }
        public void Remove_SourceLight()
        {
            Pen pen = new Pen(__backgrdColor, __pen.Width);
            __sourceLight.Remove(__graphics, __backgrdColor);
            Draw_Arrow2(pen, new PointF(__sourceLight.X, __sourceLight.Y), new PointF(0, 1), 30);
            Draw_Arrow2(pen, new PointF(__sourceLight.X, __sourceLight.Y), new PointF(1, 0), 30);
            __sourceLight.Visible = false;
        }
        public int AnyPoint_Selected()
        {
            if (__points != null)
            {
                for (int i = 0; i < __points.Count; i++)
                {
                    if (__points.ElementAt(i).IsChoosen())
                    {
                        return i;
                    }
                }
            }
            if (__sourceLight.Visible && __sourceLight.IsChoosen() && __sourceLight.Visible) { return -1; }
            if (__direction_point.Visible && __direction_point.IsChoosen() && __direction_point.Visible) { return -2; }
            return -3;
        }
        /// <summary>
        /// Xac dinh xem 1 diem nao do dang nam o phia tren hay phia duoi cua duong SPLINE
        /// </summary>
        /// <param name="point"></param>
        /// <returns>true-up, false-down</returns>
        public void Draw_Arrow2(Pen pen, PointF center, PointF vector, float length)
        {
            float ar_length = length / 5;
            float ar_high = ar_length / 3;
            PointF start, end, p1, p2, p3, p4;
            double alpha;
            if (vector.X < Complex.Epsilon && vector.X > -Complex.Epsilon)
            {
                alpha = Math.PI / 2;
            }
            else
            {
                alpha = Math.Atan(vector.Y / vector.X);
            }
            float dx = (length * (float)Math.Cos(alpha)), dy = (length * (float)Math.Sin(alpha));
            double beta = Math.Atan(ar_high / ar_length);
            double beta1 = alpha - beta;
            double beta2 = alpha + beta;
            //ar_length = Math.Sqrt(ar_length )
            start = new PointF(center.X + dx, center.Y + dy);
            p1 = new PointF((float)(start.X - ar_length * Math.Cos(beta1)), (float)(start.Y - ar_length * Math.Sin(beta1)));
            p2 = new PointF((float)(start.X - ar_length * Math.Cos(beta2)), (float)(start.Y - ar_length * Math.Sin(beta2)));
            __graphics.DrawLine(pen, start, p1);
            __graphics.DrawLine(pen, start, p2);
            __graphics.DrawLine(pen, p1, p2);

            end = new PointF(center.X - dx, center.Y - dy);
            p3 = new PointF((float)(end.X + ar_length * Math.Cos(beta1)), (float)(end.Y + ar_length * Math.Sin(beta1)));
            p4 = new PointF((float)(end.X + ar_length * Math.Cos(beta2)), (float)(end.Y + ar_length * Math.Sin(beta2)));
            __graphics.DrawLine(pen, end, p3);
            __graphics.DrawLine(pen, end, p4);
            __graphics.DrawLine(pen, p3, p4);
 
            //__graphics.DrawLine(pen, center, start);
            __graphics.DrawLine(pen, start, end);
        }

        bool __isManualLighting = true;
        int __NumOfPoints = 1;
        float __n1 = 1.5f;
        float __n2 = 1.0f;
        float  __range = 60;
        public float N1
        {
            get { return __n1; }
            set { __n1 = value; }
        }
        public float N2
        {
            get { return __n2; }
            set { __n2 = value; }
        }
        public bool IsManual_Lighting
        {
            get { return __isManualLighting; }
            set { __isManualLighting = value; }
        }
        public int NumOfPoint
        {
            get { return __NumOfPoints; }
            set { __NumOfPoints = value; }
        }
        // Radians
        public float Range_inDegree
        {
            get { return __range; }
            set { __range = value; }
        }
        //System.IO.StreamWriter stream;
        string s = "";
        public bool GocToi(PointF start , PointF gdiem , PointF tiep_tuyen, out PointF tiaKhuc_xa)
        {
            //stream.WriteLine("Calling GocToi(PointF start , PointF gdiem , PointF tiep_tuyen, out PointF tiaKhuc_xa)");
            //stream.Write("start=[{0:0.00},{1:0.00}]. ", start.X, start.Y);
            //stream.Write("gdiem=[{0:0.00},{1:0.00}].\n", gdiem.X, gdiem.Y);
            bool ret = true;
            // up - n1 -  false
            // down - n2 - true
            // 1-->2 , n21 = n2/n1
            // 2-->1 , n12 = n1/n2
            double c_suat = __n2 / __n1;
            bool where_are_you = false;

            bool p_tuyen = false, t_tuyen = false;
            PointF phap_tuyen = new PointF(tiep_tuyen.Y, -tiep_tuyen.X);
            PointF pO = new PointF(0, 0);
            PointF tia_toi = new PointF(gdiem.X - start.X, gdiem.Y - start.Y);

            double x1 = tia_toi.X, y1 = tia_toi.Y;
            double x2 = tiep_tuyen.X, y2 = tiep_tuyen.Y;
            PointF pA = new PointF((float)(pO.X - x1), (float)(pO.Y - y1)); // AO = O- A =tiep_tuyen
            PointF pB = new PointF();

            where_are_you = __region.IsVisible(new PointF((gdiem.X + start.X) / 2, (gdiem.Y + start.Y) / 2));
            if (where_are_you) {
                c_suat = __n1 / __n2;
            }

            //s += String.Format("[{0:0.0}, {0:0.0}]\n", x1, y1);
            //s += String.Format("[{0:0.0}, {0:0.0}]\n", x2, y2);
            double wtf = Math.Sqrt((x1 * x1 + y1 * y1) * (x2 * x2 + y2 * y2));
            if (wtf < Complex.Epsilon) { throw new Exception(); }

            double alpha1 = (double)Math.Acos(Math.Abs(x1 * x2 + y1 * y2) / wtf);
            if (alpha1 > 90 || alpha1 < 0) { throw new Exception(); }

            alpha1 = (double)((Math.PI / 2 - alpha1));

            s += String.Format("{0:0.0}, ", alpha1 * 180 / Math.PI);
            double alpha2;

            double goc_toi_han = 1000;
            // Kiem tra dieu kien phan xa toan phan
            if (c_suat < 1)
            {
                goc_toi_han = (double)(Math.Asin(c_suat));
                if (alpha1 >= goc_toi_han)
                { 
                    p_tuyen = false; t_tuyen = true;
                    alpha2 = alpha1;
                }
                else
                {
                    double __a__b__ = Math.Sin(alpha1) / c_suat;
                    __a__b__ = __a__b__ > 1 ? 1 : __a__b__;
                    alpha2 = (double)(Math.Asin(__a__b__));

                    //double d_err = Math.Abs(Math.Sin(alpha1) - Math.Sin(alpha2) * c_suat);
                    //if (d_err > 0.00001) { throw new Exception(); }
                }
            }
            else
            {
                double __a__b__ = Math.Sin(alpha1) / c_suat;
                __a__b__ = __a__b__ > 1 ? 1 : __a__b__;
                alpha2 = (double)(Math.Asin(__a__b__));

                //double d_err = Math.Abs(Math.Sin(alpha1) - Math.Sin(alpha2) * c_suat);
                //if (d_err > 0.001) { throw new Exception(); }
            }

            s += String.Format("{0:0.0}, ", alpha2 * 180 / Math.PI);

            alpha2 = (double)(Math.PI / 2 - alpha2);

            //stream.WriteLine("Alpha1 = {0:0.00}. Alpha2 = {1:0.00}, goc_toi_han = {2:0.00}",  alpha1 * 180 / 3.14159,  alpha2 * 180 / 3.1415, goc_toi_han * 180 / Math.PI);

            double c_alpha2 = (double)Math.Cos(alpha2);
            double s_alpha2 = (double)Math.Sin(alpha2);
            //x2 *= 400; y2 *= 400;
            pB.X = (float)(c_alpha2 * x2 - s_alpha2 * y2);
            pB.Y = (float)(c_alpha2 * y2 + s_alpha2 * x2);
            if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
                HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
            {
                tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
                //stream.WriteLine("Im here 1: pB=[{0:0.0},{1:0.0}], tiaKhux_xa=[{2:0.00},{3:0.00}], p_tuyen={4}, t_tyuyen={5}", pB.X, pB.Y, tiaKhuc_xa.X, tiaKhuc_xa.Y, p_tuyen.ToString(), t_tuyen.ToString());
            }
            else
            {
                s_alpha2 *= -1;
                pB.X = (float)(c_alpha2 * x2 - s_alpha2 * y2);
                pB.Y = (float)(c_alpha2 * y2 + s_alpha2 * x2);
                if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
                HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
                {
                    tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
                    //stream.WriteLine("Im here 2: pB=[{0:0.0},{1:0.0}], tiaKhux_xa=[{2:0.00},{3:0.00}], p_tuyen={4}, t_tyuyen={5}", pB.X, pB.Y, tiaKhuc_xa.X, tiaKhuc_xa.Y, p_tuyen.ToString(), t_tuyen.ToString());
                }
                else
                {
                    x2 *= -1; y2 *= -1;
                    pB.X = (float)(c_alpha2 * x2 - s_alpha2 * y2);
                    pB.Y = (float)(c_alpha2 * y2 + s_alpha2 * x2);
                    if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
                        HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
                    {
                        tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
                        //stream.WriteLine("Im here 3: pB=[{0:0.0},{1:0.0}], tiaKhux_xa=[{2:0.00},{3:0.00}], p_tuyen={4}, t_tyuyen={5}", pB.X, pB.Y, tiaKhuc_xa.X, tiaKhuc_xa.Y, p_tuyen.ToString(), t_tuyen.ToString());
                    }
                    else
                    {
                        s_alpha2 *= -1;
                        pB.X = (float)(c_alpha2 * x2 - s_alpha2 * y2);
                        pB.Y = (float)(c_alpha2 * y2 + s_alpha2 * x2);
                        if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
                            HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
                        {
                            tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
                            //stream.WriteLine("Im here 4: pB=[{0:0.0},{1:0.0}], tiaKhux_xa=[{2:0.00},{3:0.00}], p_tuyen={4}, t_tyuyen={5}", pB.X, pB.Y, tiaKhuc_xa.X, tiaKhuc_xa.Y, p_tuyen.ToString(), t_tuyen.ToString());
                        }
                        else
                        {
                            tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
                            //stream.WriteLine("Im here 5: pB=[{0:0.0},{1:0.0}], tiaKhux_xa=[{2:0.00},{3:0.00}], p_tuyen={4}, t_tyuyen={5}", pB.X, pB.Y, tiaKhuc_xa.X, tiaKhuc_xa.Y, p_tuyen.ToString(), t_tuyen.ToString());
                            ret = false;
                        }
                    }
                }
            }
            //stream.WriteLine("\r\n");
            //s += "\n" + HaiPhia_duongThang(pO, phap_tuyen, pA, pB).ToString() + ", ";
            //s += HaiPhia_duongThang(pO, tiep_tuyen, pA, pB).ToString() + ", ";
            //s += where_are_you;
            s += "\r\n";
            //__graphics.FillRectangle(new SolidBrush(__backgrdColor), Width - 100, 30, 100, Height - 50);
            //__graphics.DrawString(s, new Font("Arial", 10), new SolidBrush(Color.Black), Width - 100, 30);
            if (__NumOfPoints == 1)
            __graphics.DrawString(s, new Font("Arial", 10), new SolidBrush(Color.Black), Width - 100, 30);
            return ret;
        }
        /// <summary>
        /// Kiem tra 2 diem nam pA, pB nam cung phia hay khac phia so voi duong thang
        /// Duong thang duoc xac dinh tu diem bat dau va vector chi phuong
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="lineDir"></param>
        /// <param name="pA"></param>
        /// <param name="pB"></param>
        /// <returns>True if Cung phia, false neu khac phia</returns>
        public bool HaiPhia_duongThang(PointF startPoint, PointF lineDir, PointF pA, PointF pB)
        {
            // (x-x0) y_u = (y-y0) x_u
            float uX = lineDir.X, uY = lineDir.Y;
            if (uX < Complex.Epsilon && uX > -Complex.Epsilon)
            {
                // duong thang // truc y
                return ((startPoint.X - pA.X) * (startPoint.X - pB.X) >= 0);
            }
            float a = uY / uX;
            float b = startPoint.Y - startPoint.X * a;
            float yA = a * pA.X + b;
            float yB = a * pB.X + b;
            return ((yA - pA.Y) * (yB - pB.Y) >= 0);
        }
        public bool TiaSang_Chieu(PointF startPoint, PointF lineDir, PointF[] cp1, PointF[] cp2, out PointF giao_diem,
            out PointF tiep_tuyen)
        {
            //stream.WriteLine("cAlling TiaSang_Chieu(PointF startPoint, PointF lineDir, PointF[] cp1, PointF[] cp2, out PointF giao_diem, out PointF tiep_tuyen)");
            //stream.Write("startPoint=[{0:0.00},{1:0.00}], ", startPoint.X, startPoint.Y);
            //stream.Write("lineDir=[{0:0.00},{1:0.00}]\n", lineDir.X, lineDir.Y);
            int pointCount = __points.Count;
            float dimension = float.MaxValue;
            bool bl = false, co_giao_diem = false, giao_diem_vs_bezier = false;
            //PointF
                giao_diem = new PointF(); tiep_tuyen = new PointF();
            PointF out_point1 = new PointF(), out_point2 = new PointF();
            if (pointCount >= 2)
            {
                for (int i = 0; i < cp1.Length; i++)
                {
                    bl = TimGiaoDiem(__points[i], cp1[i], cp2[i], __points[1 + i], startPoint, lineDir,
                                         out out_point1, out out_point2);
                    if (bl)
                    {
                        //_giao_voi_duong_cong_Bezier = true;
                        float dim = (startPoint.X - out_point1.X) * (startPoint.X - out_point1.X) +
                            (startPoint.Y - out_point1.Y) * (startPoint.Y - out_point1.Y);
                        if (dim < dimension)
                        {
                            co_giao_diem = true;
                            giao_diem_vs_bezier = true;
                            dimension = dim;
                            giao_diem = out_point1;
                            tiep_tuyen = out_point2;
                        }
                    }
                }
            }
            float offset = 1;
            PointF p1 = new PointF(offset, offset);
            PointF p2 = new PointF(Width - offset, offset);
            PointF p3 = new PointF(Width - offset, Height - offset);
            PointF p4 = new PointF(offset, Height - offset);

            bl = TimGiaoDiem(p1, p2, startPoint, lineDir, ref dimension, ref giao_diem, ref tiep_tuyen);
            co_giao_diem |= bl;
            giao_diem_vs_bezier &= !bl;

            bl = TimGiaoDiem(p2, p3, startPoint, lineDir, ref dimension, ref giao_diem, ref tiep_tuyen);
            co_giao_diem |= bl;
            giao_diem_vs_bezier &= !bl;

            bl = TimGiaoDiem(p3, p4, startPoint, lineDir, ref dimension, ref giao_diem, ref tiep_tuyen);
            co_giao_diem |= bl;
            giao_diem_vs_bezier &= !bl;
            
            bl = TimGiaoDiem(p4, p1, startPoint, lineDir, ref dimension, ref giao_diem, ref tiep_tuyen);
            co_giao_diem |= bl;
            giao_diem_vs_bezier &= !bl;

            if (__points.Count > 2)
            {
                p1 = new PointF(__points[0].X, __points[0].Y);
                p2 = new PointF(offset, p1.Y);
                p3 = new PointF(__points[__points.Count - 1].X, __points[__points.Count - 1].Y);
                p4 = new PointF(Width - offset, p3.Y);

                bl = TimGiaoDiem(p1, p2, startPoint, lineDir, ref dimension, ref giao_diem, ref tiep_tuyen);
                co_giao_diem |= bl;
                giao_diem_vs_bezier &= !bl;

                bl = TimGiaoDiem(p3, p4, startPoint, lineDir, ref dimension, ref giao_diem, ref tiep_tuyen);
                co_giao_diem |= bl;
                giao_diem_vs_bezier &= !bl;
            }
            if (co_giao_diem)
            {
                float r_circle = 5;
                DrawPoint(startPoint, r_circle);
                // DrawPoint(giao_diem, r_circle);
                __graphics.DrawLine(new Pen(Color.Blue, 1.5f), startPoint, giao_diem);
                //stream.WriteLine("\t giao_diem=[{0:0.00},{1:0.00}]", giao_diem.X, giao_diem.Y);
            }
            //stream.WriteLine("giao_diem_vs_bezier: " + giao_diem_vs_bezier.ToString() + "\r\n");
            if (giao_diem_vs_bezier)
            {
                //PointF vectorKhucXa = new PointF();
                //GocToi(lineDir, vec_tiep_tuyen_tai_giao_diem, out vectorKhucXa);
                //startPoint.X = giao_diem.X;
                //startPoint.Y = giao_diem.Y;
                //lineDir.X = vectorKhucXa.X;
                //lineDir.Y = vectorKhucXa.Y;
                //TiaSang_Chieu(giao_diem, vectorKhucXa, cp1, cp2);
            } 
            //__graphics.FillRectangle(new SolidBrush(Color.White), Width - 100, 30, 100, Height-50);
            //__graphics.DrawString(s, new Font("Arial", 10), new SolidBrush(Color.Black), Width - 100, 30);
            return giao_diem_vs_bezier;
        }
        public void DrawPoint2(PointF point, float r_circle = 5)
        {
            __graphics.DrawEllipse(__pen, point.X - r_circle, point.Y - r_circle, r_circle * 2, r_circle * 2);
        }
        public void DrawPoint(PointF point, float r_circle = 5)
        {
            //__graphics.DrawEllipse(__pen, point.X - r_circle, point.Y - r_circle, r_circle * 2, r_circle * 2);
        }
        /// <summary>
        /// Tim giao diem cua 1 duong Bezier bac 3 vs 1 duong thang
        /// </summary>
        /// <param name="knot1">Diem dau cua duong Bezier</param>
        /// <param name="control1">Diem control 1 cua duong Bezier</param>
        /// <param name="control2">Diem control 1 cua duong Bezier</param>
        /// <param name="knot2">Diem cuoi cua duong Bezier</param>
        /// <param name="startPoint">Diem dau cua duong thang</param>
        /// <param name="chiPhuong">Vector chi phuong cua duong thang</param>
        /// <param name="listPoint">Danh sach cac giao diem</param>
        /// <param name="listVector">Danh sach vector tiep tuyen tai cac diem do</param>
        private bool TimGiaoDiem(PointEx knot1, PointF control1, PointF control2, PointEx knot2, PointF startPoint, PointF chiPhuong, out PointF point, out PointF vector)
        {
            bool ret = false;
            // Phuong trinh duong cong bac 3 Bezier
            // B(t) = (1-t)^3.P0 + 3(1-t)^2.t.C1 + 3(1-t).t^2.C2 + t^3.P3
            // B(t) = t^3.A3 + t^2.A2 + t.A1 + A0
            // A3 = - P0 + 3 C1 - 3 C2 + P3
            // A2 = 3 P0 - 6 C1 + 3 C2
            // A1 =-3 P0 + 3 C1
            // A0 = P0
            // t: 0..1, t = (x-P0.x)/(P3.x-P0.x)=(y-P0.y)/(P3.y-P0.y)
            // Bt <=> X = t^3.A3.X + t^2.A2.X + t.A1.X + A0.X
            // Bt <=> Y = t^3.A3.Y + t^2.A2.Y + t.A1.Y + A0.Y
            PointF A3 = new PointF(-knot1.X + 3 * control1.X - 3 * control2.X + knot2.X,
                                   -knot1.Y + 3 * control1.Y - 3 * control2.Y + knot2.Y);
            PointF A2 = new PointF(3 * knot1.X - 6 * control1.X + 3 * control2.X,
                                   3 * knot1.Y - 6 * control1.Y + 3 * control2.Y);
            PointF A1 = new PointF(-3 * knot1.X + 3 * control1.X,
                                   -3 * knot1.Y + 3 * control1.Y);
            PointF A0 = new PointF(knot1.X, knot1.Y);
            Complex[] roots;
            int ind = 0;
            double[] t = new double[3];
            // Duong thang co diem goc vs vector chi phuong co phuong trinh
            // y_u(x-x0) = x_u(y-y0)
            double x_u = chiPhuong.X, y_u = chiPhuong.Y;
            // x_u == 0 
            // x = x0 // truc y
            if (x_u < Complex.Epsilon && x_u > -Complex.Epsilon)
            {
                Complex.SolveEq(A3.X, A2.X, A1.X, A0.X - startPoint.X, out roots);
            }
            // y_u(x-x0) = x_u(y-y0)
            // y = y0 + y_u(x-x0)/x_u
            // y = ax + b ==> a = y_u/x_u, b = y0 -a*x0
            // y - ax - b = 0;
            else
            {
                double a = y_u / x_u;
                double b = startPoint.Y - a * startPoint.X;
                Complex.SolveEq(A3.Y - a * A3.X, A2.Y - a * A2.X, A1.Y - a * A1.X, A0.Y - a * A0.X - b, out roots);
            }
            for (int i = 0; i < roots.Length; i++)
            //for (int i = 0; i < 3; i++)
            {
                // Giao diem nam tren doan duong cong Bezier
                if (roots[i].IsReal && roots[i].Re >= 0.0 && roots[i].Re <= 1.0)
                {
                    t[ind++] = roots[i].Re;
                }
            }
            float dimension = float.MaxValue;
            point = new PointF();
            vector = new PointF(1, 1);
            for (int i = 0; i < ind; i++)
            {
                float tt = (float)t[i];
                float tt2 = tt * tt;
                float tt3 = tt2 * tt;
                float x_tmp = tt3 * A3.X + tt2 * A2.X + tt * A1.X + A0.X;
                float y_tmp = tt3 * A3.Y + tt2 * A2.Y + tt * A1.Y + A0.Y;
                // kiem tra xem co cung phuong voi vector chi phuong hay ko?
                float sub_x = x_tmp - startPoint.X;
                float sub_y = y_tmp - startPoint.Y;
                if (sub_x * x_u >= 0 && sub_y * y_u >= 0)
                {
                    float dim = sub_x * sub_x + sub_y * sub_y;
                    // lay diem nao co vi tri gan voi diem startPoint nhat
                    if (dim < dimension && dim > 0.1f) 
                    {
                        float diff_x = 3 * tt2 * A3.X + 2 * tt * A2.X + A1.X;
                        float diff_y = 3 * tt2 * A3.Y + 2 * tt * A2.Y + A1.Y;
                        point = new PointF(x_tmp, y_tmp);
                        vector = new PointF(diff_x, diff_y);
                        ret = true;
                    }
                    dimension = dim;
                }
            }
            return (ret);
        }
        private bool TimGiaoDiem(PointF start, PointF end, PointF startPoint, PointF chiPhuong, ref float dimension, ref PointF point, ref PointF vector)
        {
            // (x-x0) * uY = (y-y0) * uX
            float uX = end.X - start.X;
            float uY = end.Y - start.Y;
            float vX = chiPhuong.X, vY = chiPhuong.Y;
            float uv = uX * vY - uY * vX;
            //  2 truc song song voi nhau
            if (uv < Complex.Epsilon && uv > -Complex.Epsilon)
            {
                return false;
            }
            float x, y;
            // Song song voi truc y
            if (uX < Complex.Epsilon && uX > -Complex.Epsilon)
            {
                x = start.X;
                y = startPoint.Y + (x - startPoint.X) * (vY / vX);
            }
            else
            {
                // song song voi truc x
                if (uY < Complex.Epsilon && uY > -Complex.Epsilon)
                {
                    y = start.Y;
                    x = startPoint.X + (y - startPoint.Y) * (vX / vY);
                }
                // hai truc cat nhau, va khong song song voi truc toa do nao het
                else
                {
                    // // (x-x0) * uY = (y-y0) * uX; y = ax + b;
                    float a1, b1, a2, b2;
                    a1 = uY / uX; b1 = start.Y - start.X * a1;
                    a2 = vX / vY; b2 = startPoint.Y - startPoint.X * a2;
                    x = (b2 - b1) / (a1 - a2);
                    y = a1 * x + b1;
                }
            }
            // Kiem tra cung phuong voi duong thang
            if (((x - startPoint.X )* vX >=0) && ((y- startPoint.Y)*vY >= 0)){
                // Giao diem phai nam giua 2 diem start va end
                if ((x - start.X) * (x - end.X) <= 0 && ((y - start.Y) * (y - end.Y)) <= 0)
                {
                    // kiem tra khoang cach so voi diem khac
                    float dx = x - startPoint.X, dy = y - startPoint.Y;
                    float dim = dx * dx + dy * dy;
                    if (dim < dimension)
                    {
                        dimension = dim;
                        //point = new PointF()
                        point.X = x; point.Y = y;
                        vector.X = uX; vector.Y = uY;
                        return true;
                    }
                }
            }
            return false;
        }
    }

}


//public void GocToi(PointF start, PointF gdiem, PointF tiep_tuyen, out PointF tiaKhuc_xa)
//        {
//            float c_suat = __n2 / __n1;
//            bool where_are_you = true;

//            bool p_tuyen = false, t_tuyen = false;
//            PointF phap_tuyen = new PointF(tiep_tuyen.Y, -tiep_tuyen.X);
//            PointF pO = new PointF(0, 0);

//            //float x1 = tia_toi.X, y1 = tia_toi.Y;
//            //float x2 = tiep_tuyen.X, y2 = tiep_tuyen.Y;
//            //if (x1 != 0)
//            //{
//            //    y1 /= x1; x1 = 1;
//            //}
//            //else
//            //{
//            //    x1 /= y1; y1 = 1;
//            //}
//            //PointF pA = new PointF(1 * (pO.X - tia_toi.X), 1 * (pO.Y - tia_toi.Y)); // AO = O- A =tiep_tuyen
//            //PointF pB = new PointF();
//            PointF tia_toi = new PointF(gdiem.X - start.X, gdiem.Y - start.Y);
//            float x1 = tia_toi.X, y1 = tia_toi.Y;
//            float x2 = tiep_tuyen.X, y2 = tiep_tuyen.Y;
//            PointF pA = new PointF(1 * (pO.X - tia_toi.X), 1 * (pO.Y - tia_toi.Y)); // AO = O- A =tiep_tuyen
//            PointF pB = new PointF();

//            //where_are_you = Xacdinh_bo(new PointF(gdiem.X + pA.X, gdiem.Y + pA.Y));
//            where_are_you = !__region.IsVisible(new PointF(gdiem.X + pA.X, gdiem.Y + pA.Y));
//            if (!where_are_you) {
//                c_suat = __n1 / __n2;
//            }
            
//            //s += String.Format("[{0:0.0}, {0:0.0}]\n", x1, y1);
//            //s += String.Format("[{0:0.0}, {0:0.0}]\n", x2, y2);

//            float alpha1 = (float)Math.Acos(Math.Abs(x1 * x2 + y1 * y2) /
//                Math.Sqrt((x1 * x1 + y1 * y1) * (x2 * x2 + y2 * y2)));
//            alpha1 = (float)((Math.PI / 2 - alpha1));
//            s += String.Format("{0:0.0}, ", alpha1 * 180 / Math.PI);
//            float alpha2;

//            float goc_toi_han = float.MaxValue;
//            // Kiem tra dieu kien phan xa toan phan
//            if (c_suat < 1)
//            {
//                goc_toi_han = (float)(Math.Asin(c_suat));
//            }
//            if (alpha1 > goc_toi_han)
//            {
//                p_tuyen = false; t_tuyen = true;
//                alpha2 = (float)Math.PI / 2 - alpha1;
//                //s += "Wth\t\n";
//            }
//            else
//            {
//                alpha2 = (float)Math.Asin(Math.Sin(alpha1) / c_suat);
//                alpha2 = (float)Math.PI / 2 - alpha2;
//            }

//            s += String.Format("{0:0.0}, ", alpha2 * 180 / Math.PI);
//            float c_alpha2 = (float)Math.Cos(alpha2);
//            float s_alpha2 = (float)Math.Sin(alpha2);

//            pB.X = c_alpha2 * x2 - s_alpha2 * y2;
//            pB.Y = c_alpha2 * y2 + s_alpha2 * x2;
//            if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
//                HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
//            {
//                tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
//                s += "1";
//            }
//            else
//            {
//                s_alpha2 *= -1;
//                pB.X = c_alpha2 * x2 - s_alpha2 * y2;
//                pB.Y = c_alpha2 * y2 + s_alpha2 * x2;
//                if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
//                HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
//                {
//                    tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
//                    s += "2";
//                }
//                else
//                {
//                    x2 *= -1; y2 *= -1;
//                    pB.X = c_alpha2 * x2 - s_alpha2 * y2;
//                    pB.Y = c_alpha2 * y2 + s_alpha2 * x2;
//                    if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
//                        HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
//                    {
//                        tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
//                        s += "3";
//                    }
//                    else
//                    {
//                        s_alpha2 *= -1;
//                        pB.X = c_alpha2 * x2 - s_alpha2 * y2;
//                        pB.Y = c_alpha2 * y2 + s_alpha2 * x2;
//                        if (HaiPhia_duongThang(pO, tiep_tuyen, pA, pB) == t_tuyen &&
//                            HaiPhia_duongThang(pO, phap_tuyen, pA, pB) == p_tuyen)
//                        {
//                            tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
//                            s += "4";
//                            //tiaKhuc_xa = new PointF(pB.X, pB.Y);
//                        }
//                        else
//                        {
//                            tiaKhuc_xa = new PointF(-pO.X + pB.X, -pO.Y + pB.Y);
//                            s += "5";
//                        }
//                    }
//                }
//            }
//            //s += "\n" + HaiPhia_duongThang(pO, phap_tuyen, pA, pB).ToString() + ", ";
//            //s += HaiPhia_duongThang(pO, tiep_tuyen, pA, pB).ToString() + ", ";
//            s += where_are_you + "\r\n";
//            __graphics.FillRectangle(new SolidBrush(Color.White), Width - 100, 30, 100, Height - 50);
//            __graphics.DrawString(s, new Font("Arial", 10), new SolidBrush(Color.Black), Width - 100, 30);
//        }