﻿namespace BezierSpline_2
{
     partial class PropertiesDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_n1 = new System.Windows.Forms.TextBox();
            this.txt_n2 = new System.Windows.Forms.TextBox();
            this.txt_NumOfPoint = new System.Windows.Forms.TextBox();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Ok = new System.Windows.Forms.Button();
            this.ck_ManualLighting = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Range_max = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chiet suat 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Chiet suat 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 142);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Number of Point";
            // 
            // txt_n1
            // 
            this.txt_n1.Location = new System.Drawing.Point(130, 31);
            this.txt_n1.Margin = new System.Windows.Forms.Padding(2);
            this.txt_n1.Name = "txt_n1";
            this.txt_n1.Size = new System.Drawing.Size(131, 20);
            this.txt_n1.TabIndex = 3;
            this.txt_n1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_n2
            // 
            this.txt_n2.Location = new System.Drawing.Point(130, 68);
            this.txt_n2.Margin = new System.Windows.Forms.Padding(2);
            this.txt_n2.Name = "txt_n2";
            this.txt_n2.Size = new System.Drawing.Size(131, 20);
            this.txt_n2.TabIndex = 4;
            this.txt_n2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_NumOfPoint
            // 
            this.txt_NumOfPoint.Location = new System.Drawing.Point(130, 140);
            this.txt_NumOfPoint.Margin = new System.Windows.Forms.Padding(2);
            this.txt_NumOfPoint.Name = "txt_NumOfPoint";
            this.txt_NumOfPoint.Size = new System.Drawing.Size(131, 20);
            this.txt_NumOfPoint.TabIndex = 5;
            this.txt_NumOfPoint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Cancel.Location = new System.Drawing.Point(46, 240);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(73, 25);
            this.btn_Cancel.TabIndex = 6;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Ok
            // 
            this.btn_Ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_Ok.Location = new System.Drawing.Point(140, 240);
            this.btn_Ok.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Ok.Name = "btn_Ok";
            this.btn_Ok.Size = new System.Drawing.Size(73, 25);
            this.btn_Ok.TabIndex = 7;
            this.btn_Ok.Text = "OK";
            this.btn_Ok.UseVisualStyleBackColor = true;
            this.btn_Ok.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // ck_ManualLighting
            // 
            this.ck_ManualLighting.AutoSize = true;
            this.ck_ManualLighting.Location = new System.Drawing.Point(41, 106);
            this.ck_ManualLighting.Margin = new System.Windows.Forms.Padding(2);
            this.ck_ManualLighting.Name = "ck_ManualLighting";
            this.ck_ManualLighting.Size = new System.Drawing.Size(101, 17);
            this.ck_ManualLighting.TabIndex = 8;
            this.ck_ManualLighting.Text = "Manual Lighting";
            this.ck_ManualLighting.UseVisualStyleBackColor = true;
            this.ck_ManualLighting.CheckedChanged += new System.EventHandler(this.ManualLight_CheckChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 180);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Range";
            // 
            // txt_Range_max
            // 
            this.txt_Range_max.Location = new System.Drawing.Point(130, 177);
            this.txt_Range_max.Margin = new System.Windows.Forms.Padding(2);
            this.txt_Range_max.Name = "txt_Range_max";
            this.txt_Range_max.Size = new System.Drawing.Size(131, 20);
            this.txt_Range_max.TabIndex = 11;
            this.txt_Range_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PropertiesDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 276);
            this.Controls.Add(this.txt_Range_max);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ck_ManualLighting);
            this.Controls.Add(this.btn_Ok);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.txt_NumOfPoint);
            this.Controls.Add(this.txt_n2);
            this.Controls.Add(this.txt_n1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PropertiesDlg";
            this.Text = "Properties";
            this.Load += new System.EventHandler(this.Properties_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_n1;
        private System.Windows.Forms.TextBox txt_n2;
        private System.Windows.Forms.TextBox txt_NumOfPoint;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Ok;
        private System.Windows.Forms.CheckBox ck_ManualLighting;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Range_max;
    }
}