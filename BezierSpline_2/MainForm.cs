﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BezierSpline_2
{
    public partial class MainForm : Form
    {
        GraphicEnvironment __grapEnv;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // Color.Azure
            __grapEnv = new GraphicEnvironment(pictureBox1.CreateGraphics(), new PointF(),
               pictureBox1.BackColor);
            __grapEnv.Width = pictureBox1.Width;
            __grapEnv.Height = pictureBox1.Height;

        }
        private void On_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                __grapEnv.AddPoint(new PointF(e.X, e.Y));
            }
        }
        private void On_MouseDown(object sender, MouseEventArgs e)
        {
            // Left Mouse Button Clicked
            if (e.Button == MouseButtons.Left)
            {
                __grapEnv.LeftMouse_Down(new PointF(e.X, e.Y));
                __grapEnv.Render();
            }
            else if (e.Button == MouseButtons.Right)
            {
                Point p = PointToScreen(new Point(e.X, e.Y));
                deleteToolStripMenuItem.Enabled = __grapEnv.AnyPoint_Selected() >= -2;
                addSourceLightToolStripMenuItem.Enabled = !__grapEnv.SourceLight.Visible;
                adddirectionToolStripMenuItem.Enabled = !__grapEnv.DiectionPoint.Visible;
                pointContextMenu.Show(p);
            }
        }

        private void On_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                __grapEnv.LeftMouse_Up();
            }
        }
        PointF mousePos = new PointF();
        int __delay = 0;
        string s_track = "";
        private void On_MouseMove(object sender, MouseEventArgs e)
        {
            mousePos.X = e.X; mousePos.Y = e.Y;
            __grapEnv.Drag(new PointF(e.X, e.Y));
           if (++__delay > 10)
            {
                __delay = 0;
                //__grapEnv.__graphics.DrawString(s_track, new Font("Arial", 10), new SolidBrush(Color.White), 10, __grapEnv.Height - 50);
                s_track = "[" + e.X + "," + e.Y + "]";
                __grapEnv.__graphics.FillRectangle(new SolidBrush(__grapEnv.__otherColor), 10, __grapEnv.Height - 150, 100, 20);
                __grapEnv.__graphics.DrawString(s_track, new Font("Arial", 10), new SolidBrush(Color.Black), 10, __grapEnv.Height - 150);
            }
        }

        private void On_Paint(object sender, PaintEventArgs e)
        {
            //__grapEnv.Render();
        }

        private void On_Size(object sender, EventArgs e)
        {
            if (__grapEnv != null)
            {
                __grapEnv.RefreshGraph(pictureBox1.CreateGraphics());
                __grapEnv.Width = pictureBox1.Width;
                __grapEnv.Height = pictureBox1.Height;
            }
        }

        private void On_PointRemove(object sender, EventArgs e)
        {
            __grapEnv.Remove_PointSelected();
        }

        private void On_CleanScreen(object sender, EventArgs e)
        {
            __grapEnv.CleanScreen();
        }

        private void On_AddSourceLight(object sender, EventArgs e)
        {
            if (__grapEnv.SourceLight.Visible) { return; }
            //if (__grapEnv.SourceLight.Image_s == null)
            //{
            //    __grapEnv.SourceLight.Image_s = BezierSpline_2.Properties.Resources.light;
            //}
            //__grapEnv.SourceLight.Image_s.RotateFlip(RotateFlipType.Rotate90FlipX);
            __grapEnv.SourceLight.X = mousePos.X;
            __grapEnv.SourceLight.Y = mousePos.Y;
            __grapEnv.SourceLight.Visible = true;
            __grapEnv.Draw_SourceLight();
        }
        private void adddirectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (__grapEnv.DiectionPoint.Visible) { return; }
            __grapEnv.DiectionPoint.X = mousePos.X;
            __grapEnv.DiectionPoint.Y = mousePos.Y;
            __grapEnv.DiectionPoint.Visible = true;
            __grapEnv.Draw_DirectionPoint();
        }
        private void OnProperties_Click(object sender, EventArgs e)
        {
            PropertiesDlg dlg = new PropertiesDlg(__grapEnv.N1, __grapEnv.N2,
                __grapEnv.IsManual_Lighting, __grapEnv.NumOfPoint, (float)(__grapEnv.Range_inDegree));
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                __grapEnv.N1 = dlg.N1;
                __grapEnv.N2 = dlg.N2;
                __grapEnv.IsManual_Lighting = dlg.IsManual_Lighting;
                __grapEnv.NumOfPoint = dlg.NumOfPoint;
                __grapEnv.Range_inDegree = (float)Math.Abs(dlg.Range);
            }
        }
    }
}
